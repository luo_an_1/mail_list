#include<stdio.h>
#include<stdlib.h>
#include<string.h>
struct Person {
	int num;
	char name[50];
	char phone[20];
};
struct Person a[50];
static int count = 0, all = 50;
void menu();//菜单
void topic();//显示
void rank();//排序
void addPerson();//添加
void delPerson();//删除
void changePerson();//修改
void* findPerson();//查找
int find(int value);//方便比较输入值与数组中的值是否相等
void topic()//显示
{
	printf("==========通讯录==========\n\n\n");
	printf("==========界面==========\n");
	printf("人数:   %2d 人      | 剩余空间:   %2d 人\n", count, all);
	for (int i = 0; i < count; i++)
	{
		printf(" 编号:      %2d|姓名:       %s |电话:      %s\n", a[i].num, a[i].name, a[i].phone);
	}
}
void menu()//菜单
{
	int n;//n为操作序号
	while (1)
	{
		topic();
		printf("\n\n操作列表：\n1)排序       2)添加       3)删除\n");
		printf("4)修改       5)查找       6)退出程序\n");
		printf("\n请输入操作:");
		scanf("%d", &n);
		putchar('\n');
		putchar('\n');
		switch (n)
		{
		case 1:rank(); system("cls"); break;
		case 2:addPerson(); system("cls"); break;
		case 3:delPerson(); system("cls"); break;
		case 4:changePerson(); system("cls"); break;
		case 5:findPerson(); system("cls"); break;
		case 6:printf("程序结束.........\n"); return 0; break;
		default:printf("Error!!!\n错误操作指令, 请重新输入\n"); system("pause"); system("cls"); break;
		}
	}
}
void rank()//排序
{
	printf("选择你排序的方式:\n");
	printf("1)编号排序    2)姓名排序\n");
	int index;//输入序号选择排序方式
	scanf("%d", &index);
	struct Person temp;//中介
	switch (index)
	{
	case 1:
		for (int i = 0; i < count - 1; i++)
		{
			for (int j = 0; j < count - 1 - i; j++)
			{
				if (a[j].num > a[j + 1].num)
				{
					temp = a[j];
					a[j] = a[j + 1];
					a[j + 1] = temp;
				}
			}
		}
		break;
	case 2:
		for (int i = 0; i < count - 1; i++)
		{
			for (int j = 0; j < count - 1 - i; j++)
			{
				if (strcmp(a[j].name, a[j + 1].name) > 0)
				{
					temp = a[j];
					a[j] = a[j + 1];
					a[j + 1] = temp;
				}
			}
		}
		break;
	default:printf("Error!!!\n错误操作指令, 请重新输入\n"); system("pause"); system("cls"); break;
	}

}
void addPerson()//添加
{
	if (all == 0)
	{
		printf("\n通讯录已满\n");
		system("pause");
		return 0;
	}
	printf("添加操作:\n");
	printf("请输入添加位置:");
	scanf("%d", &a[count].num);
	if (a[count].num > 50 || a[count].num < 1)
	{
		printf("\n处理编号超过阈值\n");
		system("pause");
		return 0;
	}
	for (int i = 0; i < count; i++)
	{
		if (a[i].num == a[count].num)
		{
			printf("\n此处已有数据\n");
			system("pause");
			return 0;
		}
	}
	printf("请输入联系人姓名:");
	scanf("%s", a[count].name);
	printf("请输入联系人电话:");
	scanf("%s", a[count].phone);
	count++;
	all--;
}
void delPerson()//删除
{
	int value;//输入一个位置方便与结构数组中的元素比较
	int index;//接收值查人
	printf("删除操作:\n");
	printf("请输入删除位置:");
	scanf("%d", &value);
	if (value < 1 || value>50)
	{
		printf("\n处理编号超过阈值\n");
		system("pause");
		return 0;
	}
	index = find(value);
	if (index == -1)
	{
		printf("\n此处无数据\n");
		system("pause");
		return 0;
	}
	else
	{
		for (int i = index; i < count; i++)
		{
			a[i - 1] = a[i];
		}
		count--;
		all++;
	}
}
void changePerson()//修改
{
	int value;//输入编号
	int index;//接收值查人
	char* p1 = NULL;//用指针修改指定位置信息
	char* p2 = NULL;
	printf("修改操作:\n");
	printf("请输入修改位置:");
	scanf("%d", &value);
	if (value < 1 || value>50)
	{
		printf("\n处理编号超过阈值\n");
		system("pause");
		return 0;
	}
	index = find(value);
	if (index == -1)
	{
		printf("\n此处无数据\n");
		system("pause");
		return 0;
	}
	else
	{
		p1 = a[index - 1].name;
		p2 = a[index - 1].phone;
		printf("已擦除原有信息，请重新键入\n\n");
		printf("请输入联系人姓名:");
		scanf("%s", p1);
		printf("请输入联系人电话:");
		scanf("%s", p2);
	}
}
void* findPerson()//查找
{
	int min = 0, max = count - 1, mid;
	char input[40];
	printf("请输入你要查找联系人的姓名或电话号码:");
	scanf("%s", input);
	if (input[0] >= '1' && input[0] <= '9')
	{
		for (int i = 0; i < count; i++)
		{
			if (strcmp(input, a[i].phone) == 0)
			{
				printf(" 编号:      %2d|姓名:       %s |电话:      %s\n", a[i].num, a[i].name, a[i].phone);
				system("pause");
				return 0;
			}
		}
		printf("\n查无此人\n");
		system("pause");
	}
	else
	{
		for (int i = 0; i <= 50; i++)
		{
			if (strcmp(a[i].name, input) == 0)
			{
				getchar();
				printf(" 编号:      %2d|姓名:       %s |电话:      %s\n", a[i].num, a[i].name, a[i].phone);
				system("pause");
				return 0;
			}
		}
		printf("\n查无此人\n");
		system("pause");
	}
}
int find(int value)//方便比较输入值与数组中的编号相等的情况
{
	for (int i = 0; i < count; i++)
	{
		if (a[i].num == value)
		{
			return i + 1;
		}
	}
	return -1;
}
int main(int argc, char const* argv[])
{
	menu();
}

